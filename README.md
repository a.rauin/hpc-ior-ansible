# hpc-ior-ansible

This small ansible playbook will configure [ior](https://github.com/hpc/ior) on your home directory on the RWTH CLAIX 2018 cluster.
It will also create slurm files that you will have to manually submit.


## Getting started

- Make sure ansible is installed.
- Set the variables in the *config.yml*
- Then you can run the playbook with: 
 ```
 ansible-playbook main.yml
 ```
 